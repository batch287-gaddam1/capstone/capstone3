import {useState, useContext, useEffect} from 'react';

import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import {useParams, Link, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function ProductView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	// useParams() is a hook that will allows us to retrieve courseId passed via URL params
	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	useEffect(() => {
		console.log(productId);

		fetch(`https://capstone-2-gaddam.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	const enroll = (productId) => {
		fetch('https://capstone-2-gaddam.onrender.com/users/checkOut', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId:productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Successfully Placed order",
					icon: "success",
					text: "You have successfully enrolled for this course."
				})

				navigate("/home")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	};

	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        <Card.Subtitle>Class Schedule:</Card.Subtitle>
					        <Card.Text>Available</Card.Text>
					        
					        {
					        	(user.id !== null) ?
					        		<Button variant="primary" onClick={() => enroll(productId)} >Enroll</Button>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Enroll</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}