import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { useState, useContext }  from 'react';
import UserContext from '../UserContext';

import { NavLink, Link } from 'react-router-dom';

function NavScroll() {

  // const [ user, setUser ] = useState(localStorage.getItem('email'));

  const { user } = useContext(UserContext);

  console.log(user);

  return (
    <Navbar expand="lg" className="bg-info">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">E-Commerce</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Nav.Link as={NavLink} to="/products">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
            <NavDropdown title="More" id="navbarScrollingDropdown">
              <NavDropdown.Item as={NavLink} to="/notifications">Notifications</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/downloadApp">
                Download App
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item as={NavLink} to="/help" className="bg-secondary">
                <b>Need Help! - 24x7 Customer Care</b>
              </NavDropdown.Item>
            </NavDropdown>
            {(user.id !== null) ?
              <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              :
              <>
              <Nav.Link as={NavLink} to="/login" >
                Login
              </Nav.Link>

              <Nav.Link as={NavLink} to="/register" >
                Register
              </Nav.Link>
              </>
            }
            
              
          </Nav>
          <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavScroll;