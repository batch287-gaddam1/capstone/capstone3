
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import Scroll from './components/Scroll';
import Footer from './components/Footer';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import Help from './pages/Help';
import Notifications from './pages/Notifications';
import DownloadApp from './pages/DownloadApp';
import Cart from './pages/Cart';
import Home from './pages/Home';

import './App.css';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';

export default function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  console.log(user);

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    
    // fetch(`https://capstone-2-gaddam.onrender.com/users/details`, {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [] )

  console.log(user);

  return (
    <>
    <UserProvider >
      <Router>
        <AppNavbar/>
        <Scroll/>
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/help" element={<Help />} />
            <Route path="/notifications" element={<Notifications />} />
            <Route path="/downloadApp" element={<DownloadApp />} />
            <Route path="/*" element={<Help />} />
            
          </Routes>
        </Container>
        <Footer />
      </Router>
    </UserProvider >
    </>
  );
}

// export default App;
