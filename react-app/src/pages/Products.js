// import productsData from '../data/productsData';
import ProductCard from '../components/ProductCard';
import { useState, useEffect } from 'react';


export default function Products() {

	const [ products, setProducts ] = useState([]);

	// console.log(productsData);

	// const Products = productsData.map(product => {
	// 	return (
	// 		<ProductCard key={product.id} product={product} />
	// 	);
	// });

	useEffect(() => {
		fetch(`https://capstone-2-gaddam.onrender.com/products/active-products`)
			.then(res => res.json())//converting response into json format
			.then(data => {
				console.log(data);

				setProducts(data.map(product => {
					return(
						<ProductCard key={product._id} product={product} />
					)
				}))
			})
	}, [])

	return(
		<>
		 
		<h1> Products </h1>
		{products}
		{/*<ProductCard product={productsData[0]} />*/}
		</>
	)
}