import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {
	const data = {
		title: 'E-Commerce Platform',
		content: "Shopping",
		destination: '/products',
		label: 'Order Now'
	};

	return(
		<>
			<Banner data={data} />
			<Highlights />
			
		</>
	)
}