
import { Form, Button } from 'react-bootstrap';

export default function Help() {
  return (
    <Form>
      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="name@example.com" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
        <Form.Label>More details regarding concerned help topic</Form.Label>
        <Form.Control as="textarea" rows={3} />
      </Form.Group>

      <Form.Group controlId="formFile" className="mb-3">
        <Form.Label>Upload file(s)</Form.Label>
        <Form.Control type="file" />
      </Form.Group>

      <Button variant="primary my-3" type="submit" id="submitBtn">Submit Request</Button>

      <p>
        To connect with customer care, kindly connect to number - 1234656789
       </p>
    </Form>
  );
}
