import { Form, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the input values
	const [ email,setEmail ] = useState('');//initial value is empty i.e., useState('')
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ mobileNo, setMobileNo ] = useState('');

	// Condition for the submit button to be enabled or not
	const [ isActive, setIsActive ] = useState();

	function registerUser(e) {
		e.preventDefault();

		// To empty these valuse once the submit button is clicked
		// setEmail("");
		// setPassword1('');
		// setPassword2('');

		// setFirstName("");
		// setLastName("");
		// setMobileNo("");

		// alert("Registration successful");

		
		// https://capstone-2-gaddam.onrender.com/users/checkEmail
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				

				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email!"
				})

				// setEmail('');
				// setPassword('');
			} else {
				//fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
				fetch('http://localhost:4004/users/register', {
				    method: "POST",
				    headers: {
				        'Content-Type': 'application/json'
				    },
				    body: JSON.stringify({
				        firstName: firstName,
				        lastName: lastName,
				        email: email,
				        mobileNo: mobileNo,
				        password: password1
				    	})
				})
				.then(res => res.json())
				.then(data => {

				    console.log(data);

				    if(data === true){

				        // Clear input fields
				        setFirstName('');
				        setLastName('');
				        setEmail('');
				        setMobileNo('');
				        setPassword1('');
				        setPassword2('');

				        Swal.fire({
				            title: 'Registration successful',
				            icon: 'success',
				            text: 'Welcome to E-Commerce!'
				        });

				        // Allows us to redirect the user to the login page after registering for an account
				        navigate("/login");

				    } else {

				        Swal.fire({
				            title: 'Something wrong',
				            icon: 'error',
				            text: 'Please try again.'   
				         });

				    };
				})
			};
		})

		// setEmail('');
		// setPassword1('');
		// setPassword2("");
		// setFirstName("");
		// setLastName("");
		// setMobileNo("");
	}

	useEffect(() => {

		if((email !== "" && password1 !== '' && password2 !== '' && mobileNo.length >= 10 ) && (password2 === password1)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email,password1,password2,firstName,lastName,mobileNo]);

	return (

		(user.id !== null) ?
		<Navigate to='/products' />
		:
		
		<Row className="justify-content-md-center">
		<Col xs={12} lg="6" md="8">
		<Form onSubmit={(e) => registerUser(e)} >
			<h1 className="p-2 m-2" align="center"> Registration</h1>

			<Form.Group controlId="firstName">
				<Form.Label> First Name </Form.Label>
				<Form.Control 
					type="String"
					placeholder="Enter First Name here"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="String"
					placeholder="Enter Last Name here"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
				
			</Form.Group>

			<Form.Group controlId="email">
				<Form.Label> Email </Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="mobileNumber">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="Number"
					placeholder="Enter Your Mobile Number here"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
				
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Confirm Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Re-enter Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{
				isActive ?
					<Button variant="primary my-3" type="submit" id="submitBtn"> Submit
					</Button>
				:
					<Button variant="danger my-3" type="submit" id="submitBtn" disabled> Submit
					</Button>
			}
			
		</Form>
		</Col>
		</Row>
	)
}